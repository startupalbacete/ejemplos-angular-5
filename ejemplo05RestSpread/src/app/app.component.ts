import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(){


    //Rest
    let arr = [5,2,3];
    
    console.log(Math.max(...arr));

    let arr2 = [7,8,9];
    console.log([...arr,...arr2]);

    //Spread
    this.prueba(1,2,4);
  }

  prueba(...arg){ //numero indeterminado de argumentos
    console.log(arg);
  }
}
