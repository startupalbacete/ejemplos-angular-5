import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(){

    //1. scope restringido al bloque

    //var myVar = 3;
    //{
    //  var myVar = 4;
    //  console.log(myVar);
    //}
    //console.log(myVar);

    let myVar = 3;
    
    {
      let myVar = 4;
      console.log(myVar);
    }

    console.log(myVar);


    //2. hoisting

    //myVar2 = 2;
    //var myVar2 = 3;

    //myVar2 = 3;
    //let myVar2 = 3;

    //3. Binding en cada interación
    // let arr = [];
    // for (var i = 0;i<10;i++){
    //   arr.push(()=>i);
    // }

    // for(let f of arr){
    //   console.log(f());
    // }

    let arr = [];
    for (let i = 0;i<10;i++){
      arr.push(()=>i);
    }

    for(let f of arr){
      console.log(f());
    }    
  }


}
