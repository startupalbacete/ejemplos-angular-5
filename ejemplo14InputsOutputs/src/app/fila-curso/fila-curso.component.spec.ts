import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilaCursoComponent } from './fila-curso.component';

describe('FilaCursoComponent', () => {
  let component: FilaCursoComponent;
  let fixture: ComponentFixture<FilaCursoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilaCursoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilaCursoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
