import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CursoComponent } from './curso/curso.component';
import { ListaCursosComponent } from './lista-cursos/lista-cursos.component';
import { FilaCursoComponent } from './fila-curso/fila-curso.component';
import { CursoPipePipe } from './curso-pipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CursoComponent,
    ListaCursosComponent,
    FilaCursoComponent,
    CursoPipePipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
