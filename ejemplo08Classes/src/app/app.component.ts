import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  metodo(){

  }

  static metodo2(){
    
  }
}


class Hija extends AppComponent{

  constructor(){
    super();
    //otras operaciones
  }

  metodo(){
    super.metodo();
    //otras operaciones
  }


}