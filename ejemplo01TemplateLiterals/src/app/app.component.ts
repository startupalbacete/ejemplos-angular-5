import { Component } from '@angular/core';

let miVariable = 4;

@Component({
  selector: 'app-root',
  template: `
    <h1>Este es mi componente Raiz</h1>
    ${miVariable}
    {{title}}
  `,
  //templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title;
  constructor(){
    setInterval(() => miVariable = Math.random(),1000);
    setInterval(() => this.title = Math.random(),1000);
  }
}
