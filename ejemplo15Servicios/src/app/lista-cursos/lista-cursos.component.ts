import { Component, OnInit } from '@angular/core';
import { CursosService } from '../cursos.service';

@Component({
  selector: 'app-lista-cursos',
  templateUrl: './lista-cursos.component.html',
  styleUrls: ['./lista-cursos.component.css']
})
export class ListaCursosComponent implements OnInit {

  cursos;


  constructor(private cursosService: CursosService) { }

  ngOnInit() {
    this.cursos = this.cursosService.getAllCursos();
  }

  log(curso) {
    console.log('Evento recibido desde el hijo', curso);
  }
}
