import { Injectable } from '@angular/core';

@Injectable()
export class CursosService {

  cursos: Array<any> = [
    {name: 'Angular 2', max_alumnos: '30', fecha: new Date()},
    {name: 'Angular 4', max_alumnos: '20', fecha: new Date()}
  ];

  constructor() { }

  getAllCursos() {
    return this.cursos;
  }

}
