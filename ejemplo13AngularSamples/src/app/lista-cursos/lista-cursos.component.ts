import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-cursos',
  templateUrl: './lista-cursos.component.html',
  styleUrls: ['./lista-cursos.component.css']
})
export class ListaCursosComponent implements OnInit {

  cursos: Array<any> = [
    {name:"Angular 2",max_alumnos:"30"},
    {name:"Angular 4",max_alumnos:"20"}
  ]

  style = {padding:'10px',backgroundColor:'red',margin:'10px'};

  constructor() { }

  ngOnInit() {
  }

}
