import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-curso',
  templateUrl: './curso.component.html',
  styleUrls: ['./curso.component.css']
})
export class CursoComponent implements OnInit {

  title:string;
  label:string;

  constructor() {
    this.title = "Angular 2";
    this.label = "Apuntarme";

    setTimeout(()=>{
      this.title = "Angular 4";
      this.label = "Quiero apuntarme";     
    },3000)
  }

  ngOnInit() {
  }

  onClick(){
    console.log("Clicked");
  }
}
