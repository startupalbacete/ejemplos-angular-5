import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(){
    let myVar = "Hola";
    let objeto = {myVar:myVar};
    console.log(objeto); //forma antigua
    let objeto2 = {myVar}; 
    console.log(objeto2); //forma nueva
  }
}
