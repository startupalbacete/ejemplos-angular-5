import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements Curso{
  title = 'app';
  public name;
  public max_alumnos;

  constructor(){

    let var1 = {name:"Curso",max_alumnos:30};//implementación implicita
    let var2 = new AppComponent();//implementacion explicita    

    this.name="Curso1";
    this.max_alumnos= 4;
  }
}

interface Curso {
  name:string,
  max_alumnos:number
}