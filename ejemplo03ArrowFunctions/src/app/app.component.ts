import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(){

    // let arr = [];
    // for(let i = 0;i<10;i++){
    //   arr.push(function(){
    //     return i
    //   });
    // }

    // for(let f of arr){
    //   console.log(f());
    // }

    let arr = [];
    for(let i = 0;i<10;i++){
      arr.push(() => i);
    }

    for(let f of arr){
      console.log(f());
    }    

  }
}
