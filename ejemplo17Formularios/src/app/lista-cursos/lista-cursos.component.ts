import { Component, OnInit } from '@angular/core';
import { CursosService } from '../cursos.service';

@Component({
  selector: 'app-lista-cursos',
  templateUrl: './lista-cursos.component.html',
  styleUrls: ['./lista-cursos.component.css']
})
export class ListaCursosComponent implements OnInit {

  cursos;


  constructor(private cursosService: CursosService) { }

  ngOnInit() {
    // this.cursos = this.cursosService.getAllCursos();
    this.cursosService.getAllCursos()
     // .subscribe(x => this.cursos = x);
     .subscribe(response => this.onSuccess(response), error =>  this.onError (error), () => this.onCompletion());
     /*.subscribe(
      response => this.cursos = response,
      error => console.log('Error', error),
      () => console.log('Completado')
    );*/
  }

  onSuccess(response: Array<any>) {
    this.cursos = response;
  }

  onError(err) {
    console.log('Error', err);
  }

  onCompletion() {
    console.log('Completado');
  }


  log(curso) {
    console.log('Evento recibido desde el hijo', curso);
  }
}
