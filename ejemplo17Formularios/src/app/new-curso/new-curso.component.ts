import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-curso',
  templateUrl: './new-curso.component.html',
  styleUrls: ['./new-curso.component.css']
})
export class NewCursoComponent implements OnInit {

  nameField;
  alumnosField;

  constructor() { }

  ngOnInit() {
  }

  onSubmit(form) {
    console.log(form);
  }
}
