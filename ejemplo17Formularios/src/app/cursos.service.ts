import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
@Injectable()
export class CursosService {

  cursos: Array<any> = [
    {name: 'Angular 2', max_alumnos: '30', fecha: new Date()},
    {name: 'Angular 4', max_alumnos: '20', fecha: new Date()}
  ];

  constructor(private http: Http) { }

  getAllCursos() {

    return this.http.get('http://my-json-server.typicode.com/jquirogaj/rest-cursos/cursos')
      .map(response => response.json());
    // return this.cursos;
  }

}
