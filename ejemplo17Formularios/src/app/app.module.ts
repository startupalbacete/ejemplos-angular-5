import { CursosService } from './cursos.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CursoComponent } from './curso/curso.component';
import { ListaCursosComponent } from './lista-cursos/lista-cursos.component';
import { FilaCursoComponent } from './fila-curso/fila-curso.component';
import { CursoPipePipe } from './curso-pipe.pipe';
import {HttpModule, Http} from '@angular/http';
import { NewCursoComponent } from './new-curso/new-curso.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    CursoComponent,
    ListaCursosComponent,
    FilaCursoComponent,
    CursoPipePipe,
    NewCursoComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [CursosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
