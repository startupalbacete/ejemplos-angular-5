import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(){
    let var1 = new Curso();
    console.log(var1);

    //console.log(var1.title); //error es privada
  }
}

class Curso {
  constructor(private title="Curso"){  
  //constructor(private title="Curso", max_alumnos?){ //definir un parametro opcional
    console.log(this.title);
  }
}