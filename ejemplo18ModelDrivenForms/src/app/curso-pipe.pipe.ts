import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cursoPipe'
})
export class CursoPipePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return 'Curso sin nombre';
  }

}
