import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  var1:any;
  var2 = 5;
  var3:number;
  var4:Array<any>;
  var5:Array<number>;
  var6:number[];

  constructor(){

    this.var1 = 1; //no hay error
    this.var1 = "texto"; // no hay error

    //tipado gradual
    //this.var2 = "texto"; //error
    this.var2 = 3; // OK

    //restringir a un tipo
    //this.var3 = "texto";//error
    this.var3 = 5; //OK

    //arrays
    this.var4 = [2,3,4,'texto']; //ok
    this.var4 = [2,3,4]; //ok
    this.var4 = ['2','3','4'];  //ok

    this.var5 = [3,4,5]; // ok 
    //this.var5 = ['3','4','5']; // error
    this.var6 = [3,4,5]; // ok 

  }
}
