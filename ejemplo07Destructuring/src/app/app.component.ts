import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(){
    let arr = [3,4,5,6];
    
    let [a,b] = arr;

    console.log("a", a);
    console.log("b", b);
    
    //destructuring junto con Rest
    let [c,...d] = arr;

    console.log("c", c);
    console.log("d", d);

    //trabajando con objetos
    let obj = {prop1:1,prop2:2,prop3:3,prop4:4};
    let {prop1,prop2} = obj;

    console.log(prop1);
    console.log(prop2);
    
    //intercambiar variables
    let var1 = 4;
    let var2 = 5;
    [var1,var2] = [var2,var1];

    console.log(var1);
    console.log(var2);    

    //transformar parametros funcion
    this.prueba([2,3]);
  }

  prueba([a,b]){
    console.log("a", a);
    console.log("b", b);
  }
}
