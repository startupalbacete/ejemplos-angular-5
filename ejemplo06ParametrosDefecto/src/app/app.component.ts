import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(){
    //this.prueba(undefined);
    this.prueba();
  }

  prueba(x = 4){
    //let y = x || 4;
    //console.log(y);
    console.log(x);
  }

}
