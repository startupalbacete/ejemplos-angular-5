import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-curso',
  templateUrl: './new-curso.component.html',
  styleUrls: ['./new-curso.component.css']
})
export class NewCursoComponent implements OnInit {

  nameControl: FormControl;
  cursoForm;

  constructor(private fb: FormBuilder) {
    this.nameControl = new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), this.myValidator]));
    this.cursoForm = this.fb.group({
      name: this.nameControl,
      max_alumnos: ''
    });

  }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.cursoForm);
  }

  myValidator(control: FormControl) {
    if ( control.value.indexOf('jQuery') !== -1) {
      return {'errorCurso': true};
    }
  }
}
