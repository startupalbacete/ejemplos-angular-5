import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewCursoComponent } from '../new-curso/new-curso.component';
import { CursoPipePipe } from '../curso-pipe.pipe';
import { CursoComponent } from '../curso/curso.component';
import { ListaCursosComponent } from '../lista-cursos/lista-cursos.component';
import { FilaCursoComponent } from '../fila-curso/fila-curso.component';
import { CursosService } from '../cursos.service';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    CursoComponent,
    ListaCursosComponent,
    FilaCursoComponent,
    CursoPipePipe,
    NewCursoComponent
  ],
  providers: [CursosService],
  exports: [
    ListaCursosComponent,
    NewCursoComponent
  ]
})
export class CursosModule { }
