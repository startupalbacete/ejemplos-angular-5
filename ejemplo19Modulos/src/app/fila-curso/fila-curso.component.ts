import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-fila-curso',
  templateUrl: './fila-curso.component.html',
  styleUrls: ['./fila-curso.component.css']
})
export class FilaCursoComponent implements OnInit {

  @Input() curso;
  @Output() cursoClicked: EventEmitter<any> = new EventEmitter();

  style = {padding: '10px' , backgroundColor: 'red', margin: '10px'};

  constructor() {
    console.log('Constructor', this.curso);
  }

  ngOnInit() {
    console.log('Init', this.curso);
  }

  onClick() {
    this.cursoClicked.emit(this.curso);
  }

}
