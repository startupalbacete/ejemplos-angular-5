import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <h1>{{curso?.nombre}}</h1>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  curso: Curso;

  constructor(){
    //this.curso = {nombre:'Angular 4'};//si comentamos sin operador elvis daría error
  }
}

interface Curso {
  nombre:string
}
