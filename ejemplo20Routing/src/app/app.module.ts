import { LoggedInGuard } from './logged-in.guard';
import { FilaCursoComponent } from './fila-curso/fila-curso.component';
import {RouterModule, Routes} from '@angular/router';
import { NewCursoComponent } from './new-curso/new-curso.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpModule, Http} from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CursosModule } from './cursos/cursos.module';
import { ListaCursosComponent } from './lista-cursos/lista-cursos.component';
import { DetalleCursoComponent } from './detalle-curso/detalle-curso.component';


const appRoutes: Routes = [
 { 'path': 'new-curso', 'component': NewCursoComponent, canActivate: [LoggedInGuard]},
 { 'path': 'cursos', 'component': ListaCursosComponent},
 { 'path': 'curso/:id', 'component': DetalleCursoComponent}
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    CursosModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [LoggedInGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
