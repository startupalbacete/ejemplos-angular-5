import { RouterModule } from '@angular/router';
import { DetalleCursoComponent } from './../detalle-curso/detalle-curso.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewCursoComponent } from '../new-curso/new-curso.component';
import { CursoPipePipe } from '../curso-pipe.pipe';
import { CursoComponent } from '../curso/curso.component';
import { ListaCursosComponent } from '../lista-cursos/lista-cursos.component';
import { FilaCursoComponent } from '../fila-curso/fila-curso.component';
import { CursosService } from '../cursos.service';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [
    CursoComponent,
    ListaCursosComponent,
    FilaCursoComponent,
    CursoPipePipe,
    NewCursoComponent,
    DetalleCursoComponent

  ],
  providers: [CursosService],
  exports: [
    ListaCursosComponent,
    NewCursoComponent
  ]
})
export class CursosModule { }
